import { Test, TestingModule } from '@nestjs/testing';
import { HttpStatus, INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { getModelToken } from '@nestjs/sequelize';
import { UsersModule } from '../src/users/users.module';
import { User } from '../src/users/entity/user.entity';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  const testUser = {
    username: 'test_username',
    password: 'test_password',
  };

  const mockUserRepository = {
    findOne: jest.fn((obj) => testUser),
    create: jest.fn((dto) => {
      return {
        ...testUser,
        id: 'test_id',
        createdAt: '123',
        updatedAt: '123',
      };
    }),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [UsersModule],
    })
      .overrideProvider(getModelToken(User))
      .useValue(mockUserRepository)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (POST)', () => {
    return request(app.getHttpServer())
      .post('/api/v1/user')
      .send({
        username: 'test_username',
        password: 'test_password',
      })
      .expect(HttpStatus.CREATED)
      .then((response) => {
        expect(response.body).toEqual({
          id: expect.any(String),
          username: 'test_username',
          password: 'test_password',
          createdAt: expect.any(String),
          updatedAt: expect.any(String),
        });
      });
  });
});
