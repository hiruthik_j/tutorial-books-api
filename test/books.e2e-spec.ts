import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { getModelToken } from '@nestjs/sequelize';
import { Book } from '../src/books/entity/books.entity';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  const testBook = {
    id: 'test_id',
    title: 'test_title',
    author: 'test_author',
    createdAt: '2022-10-31T11:29:27.000Z',
    updatedAt: '2022-10-31T11:29:27.000Z',
  };

  const mockBookRepository = {
    findByPk: jest.fn((id) => testBook),
    findAll: jest.fn(() => [testBook]),
    create: jest.fn((book) => testBook),
    destroy: jest.fn((book) => 1),
  };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    })
      .overrideProvider(getModelToken(Book))
      .useValue(mockBookRepository)
      .compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/ (GET)', () => {
    return request(app.getHttpServer())
      .get('/api/v1/book')
      .set(
        'Authorization',
        'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJoaiIsImlhdCI6MTY2NzM3MDkxMSwiZXhwIjoxNjY3NDU3MzExfQ.Vf5Cqw2e7-xzaH1XmicD-v0d1yf3t1rybB2TBWKuGgo',
      )
      .expect(200);
  });
});
