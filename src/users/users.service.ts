import { Inject, Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/sequelize';
import { CreateUserDto } from './dto/create-user.dto';
import { User } from './entity/user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User)
    private readonly usersRepository: typeof User,
  ) {}

  async getUser(username: string) {
    const user = await this.usersRepository.findOne({
      where: {
        username,
      },
    });
    return user;
  }

  async createUser(userBody: CreateUserDto) {
    return await this.usersRepository.create(userBody as any);
  }
}
