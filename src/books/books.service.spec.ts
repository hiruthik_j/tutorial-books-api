import { getModelToken } from '@nestjs/sequelize';
import { Test, TestingModule } from '@nestjs/testing';
import { BooksService } from './books.service';
import { Book } from './entity/books.entity';

describe('BooksService', () => {
  let service: BooksService;
  let model: typeof Book;
  const testBook = {
    id: 'test_id',
    title: 'test_title',
    author: 'test_author',
    createdAt: '2022-10-31T11:29:27.000Z',
    updatedAt: '2022-10-31T11:29:27.000Z',
  };
  const mockBookRepository = {
    findByPk: jest.fn((id) => testBook),
    findAll: jest.fn(() => [testBook]),
    create: jest.fn((book) => testBook),
    destroy: jest.fn((book) => 1),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BooksService,
        {
          provide: getModelToken(Book),
          useValue: mockBookRepository,
        },
      ],
    }).compile();

    service = module.get<BooksService>(BooksService);
    model = module.get<typeof Book>(getModelToken(Book));
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('should get all books', async () => {
    expect(await service.getAllBooks()).toEqual([testBook]);
  });

  it('should get a book', async () => {
    expect(await service.getBook(testBook.id)).toEqual(testBook);
  });

  it('should create a book', async () => {
    expect(
      await service.createBook({
        title: testBook.title,
        author: testBook.author,
      }),
    ).toEqual(testBook);
  });

  // it('should delete a book', async () => {
  //   expect(await service.deleteBook(testBook.id)).toEqual(1);
  // });

  // it('should update a book', async () => {
  //   const destroyStub = jest.fn();
  //   const findSpy = jest.spyOn(model, 'findByPk').mockReturnValue({
  //     destroy: destroyStub,
  //   } as any);
  //   const retVal = await service.deleteBook(testBook.id);
  //   expect(retVal).toBeUndefined();
  // });
});
