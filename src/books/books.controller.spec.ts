import { Test, TestingModule } from '@nestjs/testing';
import { BooksController } from './books.controller';
import { BooksService } from './books.service';

describe('BooksController', () => {
  let controller: BooksController;

  const mockBooksService = {
    createBook: jest.fn((dto) => {
      return {
        ...dto,
        id: 'test_str',
        createdAt: 'test-created',
        updatedAt: 'test-updated',
      };
    }),
    updateBook: jest.fn((id, dto) => {
      return {
        ...dto,
        id,
        createdAt: 'test-created',
        updatedAt: 'test-updated',
      };
    }),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BooksController],
      providers: [BooksService],
    })
      .overrideProvider(BooksService)
      .useValue(mockBooksService)
      .compile();

    controller = module.get<BooksController>(BooksController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });

  it('should create book', () => {
    const dto = {
      title: 'Test Title',
      author: 'Test Author',
    };

    expect(controller.post(dto)).toEqual({
      id: expect.any(String),
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
      ...dto,
    });

    expect(mockBooksService.createBook).toHaveBeenCalledWith(dto);
  });

  it('should update a user', () => {
    const dto = {
      title: 'Test Title',
      author: 'Test Author',
    };

    expect(controller.patch('test_id', dto)).toEqual({
      id: 'test_id',
      createdAt: expect.any(String),
      updatedAt: expect.any(String),
      ...dto,
    });

    expect(mockBooksService.updateBook).toHaveBeenCalledWith('test_id', dto);
  });
});
