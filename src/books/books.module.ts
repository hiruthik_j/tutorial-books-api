import { Module } from '@nestjs/common';
import { SequelizeModule } from '@nestjs/sequelize';
import { bookProviders } from './book.providers';
import { BooksController } from './books.controller';
import { BooksService } from './books.service';
import { Book } from './entity/books.entity';

@Module({
  controllers: [BooksController],
  providers: [BooksService, ...bookProviders],
  imports: [SequelizeModule.forFeature([Book])],
})
export class BooksModule {}
